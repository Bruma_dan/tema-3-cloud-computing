#pragma once
#include <string>
class Vehicle
{
public:
	//din laborator 
	struct Comparator {
		bool operator()(const Vehicle& left, const Vehicle& right);
	};
private:
	std::string brand;
	size_t year;
public:
	Vehicle(std::string brand, time_t year);
	~Vehicle();
};

