#include"Vehicle.h"
#include <map>
#include <iostream>
int main()
{
	std::map<Vehicle, std::string, Vehicle::Comparator>OEM;

	OEM.emplace(Vehicle("Subaru", 2000), "Subaru");
	OEM.emplace(Vehicle("Skoda", 2006), "Skoda");
	OEM.emplace(Vehicle("BMW", 2002), "BMW");
	OEM.emplace(Vehicle("Mercedes", 2003), "Mercedes");
	OEM.emplace(Vehicle("VW", 2004), "VW");

	for (const auto & element : OEM)
	{
		std::cout << element.second << '\n';
	}
	system("pause");
}