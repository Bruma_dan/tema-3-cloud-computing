#include "RequestsManager.h"

#include "HandshakeRequest.h"
#include "ZodieRequest.h"
#include "SumRequest.h"
#include "WordCountRequest.h"

RequestsManager::RequestsManager()
{
	
	this->requests.emplace("Zodie", std::make_shared<ZodieRequest>());
	
}

std::map<std::string, std::shared_ptr<Framework::Request>> RequestsManager::getMap() const
{
	return this->requests;
}
