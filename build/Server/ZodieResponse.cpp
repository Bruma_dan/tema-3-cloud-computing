#include <stdlib.h>
#include <string> 
#include <iostream>
#include<conio.h>


#include "ZodieResponse.h"

ZodieResponse::ZodieResponse() : Response("Zodie")
{

}




std::string rezultat;


bool validateDate(int luna, int ziua, int anul)
{
	if (ziua < 1 || ziua > 31) {
		rezultat = "Data invalida, zilele sunt de la 1 la 31";
		std::cout << rezultat << std::endl;
		return false;
	}
	else if (luna < 1 || luna > 12) {
		rezultat = "Data invalida, lunile sunt de la 1 la 12";
		std::cout << rezultat << std::endl;
		return false;
	}
	else if (anul < 1) {
		rezultat = "Data invalida, anul nu poate fi negativ";
		std::cout << rezultat << std::endl;
		return false;
	}
	else if (anul > 999999) {
		rezultat = "Data invalida, anul este prea mare";
		std::cout << rezultat << std::endl;
		return false;
	}

	if ((luna == 4 || luna == 6 || luna == 9 || luna == 11) && ziua > 30) {
		rezultat = "Data invalida, luna are doar 30 de zile";
		std::cout << rezultat << std::endl;
		return false;
	}
	else if ((luna == 2) && (anul % 4 == 0) && ziua > 29) {
		rezultat = "Data invalida";
		std::cout << rezultat << std::endl;
		return false;

	}
	else if ((luna == 2) && (anul % 4 != 0) && ziua > 28) {
		rezultat = "Data invalida, nu este an bisect";
		std::cout << rezultat << std::endl;
		return false;
	}

	return true;


}



std::string verificaZodie(int ziua, int luna)
{

	if (luna == 3 && ziua > 20 || luna == 4 && ziua < 21)
	{
		rezultat = "Berbec, Perioada : 21 martie - 20 aprilie";
		std::cout << rezultat << std::endl;
		return rezultat;
	}
	if (luna == 4 && ziua > 20 || luna == 5 && ziua < 22)
	{
		rezultat = "Taur, Perioada : 21 aprilie - 21 mai";
		std::cout << rezultat << std::endl;
		return rezultat;
	}
	if (luna == 5 && ziua > 21 || luna == 6 && ziua < 22)
	{
		rezultat = "Gemeni, Perioada : 22 mai - 21 iunie";
		std::cout << rezultat << std::endl;
		return rezultat;
	}
	if (luna == 6 && ziua > 21 || luna == 7 && ziua < 23)
	{
		rezultat = "Rac, Perioada : 21 iunie - 22 iulie";
		std::cout << rezultat << std::endl;
		return  rezultat;
	}
	if (luna == 7 && ziua > 22 || luna == 8 && ziua < 23)
	{
		rezultat = "Leu, Perioada : 22 iulie - 21 august";
		std::cout << rezultat << std::endl;
		return rezultat;
	}
	if (luna == 8 && ziua > 22 || luna == 9 && ziua < 23)
	{
		rezultat = "Fecioara, Perioada : 23 august - 23 septembrie";
		std::cout << rezultat << std::endl;
		return rezultat;
	}
	if (luna == 9 && ziua > 22 || luna == 10 && ziua < 23)
	{
		rezultat = "Balanta, Perioada : 23 septembrie - 22 octombrie";
		std::cout << rezultat << std::endl;
		return rezultat;
	}
	if (luna == 10 && ziua > 22 || luna == 11 && ziua < 22)
	{
		rezultat = "Scorpion, Perioada : 23 octombrie - 21 noiembrie";
		std::cout << rezultat << std::endl;
		return  rezultat;
	}
	if (luna == 11 && ziua > 21 || luna == 12 && ziua < 22)
	{
		rezultat = "Sagetator, Perioada : 22 mai - 22 iunie";
		std::cout << rezultat << std::endl;
		return rezultat;
	}
	if (luna == 12 && ziua > 21 || luna == 1 && ziua < 20)
	{
		rezultat = "Capricorn, Perioada : 22 decembrie - 19 ianuarie";
		std::cout << rezultat << std::endl;
		return rezultat;
	}
	if (luna == 1 && ziua > 19 || luna == 2 && ziua < 19)
	{
		rezultat = "Varsator, Perioada : 20 ianuarie - 18 februarie";
		std::cout << rezultat << std::endl;
		return rezultat;
	}
	if (luna == 2 && ziua > 18 || luna == 3 && ziua < 21)
	{
		rezultat = "Pesti, Perioada : 19 februarie - 20 martie";
		std::cout << rezultat << std::endl;
		return rezultat;
	}
	rezultat = "Eroare";
	return rezultat;
}



std::string ZodieResponse::interpretPacket(const boost::property_tree::ptree& packet)
{

	std::string zodie;

	int ziua = atoi(packet.get<std::string>("Ziua").c_str());
	int luna = atoi(packet.get<std::string>("Luna").c_str());
	int anul = atoi(packet.get<std::string>("Anul").c_str());

	if (validateDate(luna, ziua, anul) == true) {
		zodie = verificaZodie(ziua, luna);
	}
	else {
		zodie = rezultat;
	}



	this->content.push_back(boost::property_tree::ptree::value_type("File", "zodie.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Zodia", zodie));
	this->content.push_back(boost::property_tree::ptree::value_type("Ziua", std::to_string(ziua)));
	this->content.push_back(boost::property_tree::ptree::value_type("Luna", std::to_string(luna)));
	this->content.push_back(boost::property_tree::ptree::value_type("Anul", std::to_string(anul)));


	return this->getContentAsString();
}


