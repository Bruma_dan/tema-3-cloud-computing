#pragma once

#include "Response.h"

class ZodieResponse : public Framework::Response
{
public:
	ZodieResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};