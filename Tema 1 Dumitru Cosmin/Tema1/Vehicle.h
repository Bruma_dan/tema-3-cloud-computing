#pragma once
#include <string>

using namespace std;

class Vehicle
{
public:
	//din laborator 
	struct Comparator {
		bool operator()(const Vehicle& left, const Vehicle& right);
	};
private:
	string brand;
	int year;
	
public:
	Vehicle(string brand, int year);
	~Vehicle();
};

