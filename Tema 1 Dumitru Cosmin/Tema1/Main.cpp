#include"Vehicle.h"
#include <map>
#include <iostream>

using namespace std;


int main()
{
	map<Vehicle, string, Vehicle::Comparator>option;


	Vehicle vehicle1("BMW");
	Vehicle vehicle2("Audi", 2012);
	Vehicle vehicle3("Mercedes", 2011);
	Vehicle vehicle4("Ford", 2005);
	Vehicle vehicle5("Dacia", 2019);

	option.emplace(vehicle1, "BMW");//insert object by emplace function
	option.emplace(vehicle2, "Audi");
	option.emplace(vehicle3, "Mercedes");
	option.emplace(vehicle4, "Ford");
	option.emplace(vehicle5, "Dacia");

	for(const auto & element: option)
	{
		cout << element.second << endl;
	}
	system("pause");
}