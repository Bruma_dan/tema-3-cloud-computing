#include "ResponsesManager.h"

#include "HandshakeResponse.h"
#include "SumResponse.h"
#include "ZodieResponse.h"
#include "WordCountResponse.h"

ResponsesManager::ResponsesManager()
{
	
	this->Responses.emplace("Zodie", std::make_shared<ZodieResponse>());

}

std::map<std::string, std::shared_ptr<Framework::Response>> ResponsesManager::getMap() const
{
	return this->Responses;
}
