#pragma once
#include <string>
class Vehicle
{
public:
	//din laborator 
	struct Comparator {
		bool operator()(const Vehicle& left, const Vehicle& right);
	};
private:
	std::string brand;
	int year;
	
public:
	Vehicle(std::string brand, int year);
	~Vehicle();
};

