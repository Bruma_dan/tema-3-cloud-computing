#include "Main.h"


#include"Vehicle.h"
#include <map>
#include <iostream>
int main()
{
	std::map<Vehicle, std::string, Vehicle::Comparator>Compare;
	//Instantiate 5 objects
	Vehicle Car1("Ford", 1992);
	Vehicle Car2("Nissan", 1985);
	Vehicle Car3("Skoda", 2018);
	Vehicle Car4("Honda", 2005);
	Vehicle Car5("Mitsubishi", 2003);
	//Mapping objects
	Compare.emplace(Car1, "Ford");
	Compare.emplace(Car2, "Nissan");
	Compare.emplace(Car3, "Skoda");
	Compare.emplace(Car4, "Honda");
	Compare.emplace(Car5, "Mitsubishi");

	for (const auto & element : Compare)
	{
		std::cout << element.second << '\n';
	}
	system("pause");
}